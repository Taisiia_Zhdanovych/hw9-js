
// ## Теоретичні питання

// 1. Опишіть, як можна створити новий HTML тег на сторінці.
//    document.createElement(tag);
//    insertAdjacentElement(where, elem) - може одразу і створити і вставити елемент;


// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

//  Перший параметр insertAdjacentElement(where, elem) може приймати такі значення "beforebegin","afterend","afterbegin","beforeend".
// "beforebegin" – для вставки html безпосередньо перед elem;
// "afterbegin" – для вставки html як першого дочірнього елемента в elem;
// "beforeend" – для вставки html як останнбого дочірнього елемента в elem;
// "afterend" – для вставки html безпосередньо после elem.

// 3. Як можна видалити елемент зі сторінки?

//  Метод element.remove(), є ще removeChild(), але він 
// зустрічається вже більше в старих кодах.



// ## Завдання

// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку
// у вигляді списку. Завдання має бути виконане на чистому Javascript без використання
// бібліотек типу jQuery або React.

// #### Технічні вимоги:

// - Створити функцію, яка прийматиме на вхід масив і опціональний 
// другий аргумент parent - DOM-елемент, до якого буде прикріплений 
// список (по дефолту має бути document.body.
// - кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// Приклади масивів, які можна виводити на екран:


// ```javascript
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ```

// ```javascript
// ["1", "2", "3", "sea", "user", 23];
// ```

// - Можна взяти будь-який інший масив.







 
function createListAll(arr, list = document.body ) {     
    let createUl = document.createElement('ul');
  list.prepend(createUl);

function creatListEl(el, createUl) {

    let listEl = createUl.insertAdjacentHTML('beforeend', `<li>${el}</li>`);
    
};
arr.forEach(el => {
  
    creatListEl(el, createUl);

})
}
    

 let d = document.createElement('div'); // для прикладу
 document.body.prepend(d)




  
createListAll(["1", "2", "3", "sea", "user", 23]); 
createListAll(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], d ); 







